# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'AdminConsultants'
        db.delete_table(u'admin_consultants')

        # Deleting field 'AdminProjects.housing_type'
        db.delete_column(u'admin_projects', 'housing_type')

        # Deleting field 'AdminProjects.square_footage_range'
        db.delete_column(u'admin_projects', 'square_footage_range')

        # Deleting field 'AdminProjects.lot_size'
        db.delete_column(u'admin_projects', 'lot_size')

        # Deleting field 'AdminProjects.add_map'
        db.delete_column(u'admin_projects', 'add_map')

        # Deleting field 'AdminProjects.city'
        db.delete_column(u'admin_projects', 'city')

        # Deleting field 'AdminProjects.sales_to_date'
        db.delete_column(u'admin_projects', 'sales_to_date')

        # Deleting field 'AdminProjects.zip'
        db.delete_column(u'admin_projects', 'zip')

        # Deleting field 'AdminProjects.density'
        db.delete_column(u'admin_projects', 'density')

        # Deleting field 'AdminProjects.landscape_architect'
        db.delete_column(u'admin_projects', 'landscape_architect_id')

        # Deleting field 'AdminProjects.other_meta_words'
        db.delete_column(u'admin_projects', 'other_meta_words')

        # Deleting field 'AdminProjects.state'
        db.delete_column(u'admin_projects', 'state')

        # Deleting field 'AdminProjects.website'
        db.delete_column(u'admin_projects', 'website')

        # Deleting field 'AdminProjects.sales_start_date'
        db.delete_column(u'admin_projects', 'sales_start_date')

        # Deleting field 'AdminProjects.phone'
        db.delete_column(u'admin_projects', 'phone')

        # Deleting field 'AdminProjects.address'
        db.delete_column(u'admin_projects', 'address')

        # Deleting field 'AdminProjects.price_range'
        db.delete_column(u'admin_projects', 'price_range')

        # Adding field 'AdminProjects.consultants'
        db.add_column(u'admin_projects', 'consultants',
                      self.gf('django.db.models.fields.TextField')(default="[]"),
                      keep_default=False)

        # Adding field 'AdminProjects.location'
        db.add_column(u'admin_projects', 'location',
                      self.gf('django.db.models.fields.TextField')(default='', max_length=255),
                      keep_default=False)

        # Adding field 'AdminProjects.profile'
        db.add_column(u'admin_projects', 'profile',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'AdminProjects.meta_keywords'
        db.add_column(u'admin_projects', 'meta_keywords',
                      self.gf('main.models.TagEditField')(null=True, blank=True),
                      keep_default=False)

        # Removing M2M table for field other_consultants on 'AdminProjects'
        db.delete_table(db.shorten_name(u'admin_projects_other_consultants'))


        # Renaming column for 'AdminProjects.builder' to match new field type.
        db.rename_column(u'admin_projects', 'builder_id', 'builder')
        # Changing field 'AdminProjects.builder'
        db.alter_column(u'admin_projects', 'builder', self.gf('django.db.models.fields.CharField')(max_length=255))
        # Removing index on 'AdminProjects', fields ['builder']
        db.delete_index(u'admin_projects', ['builder_id'])


        # Changing field 'AdminProjects.project_date'
        db.alter_column(u'admin_projects', 'project_date', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Renaming column for 'AdminProjects.architect' to match new field type.
        db.rename_column(u'admin_projects', 'architect_id', 'architect')
        # Changing field 'AdminProjects.architect'
        db.alter_column(u'admin_projects', 'architect', self.gf('django.db.models.fields.CharField')(max_length=255))
        # Removing index on 'AdminProjects', fields ['architect']
        db.delete_index(u'admin_projects', ['architect_id'])


        # Renaming column for 'AdminProjects.interior_designer' to match new field type.
        db.rename_column(u'admin_projects', 'interior_designer_id', 'interior_designer')
        # Changing field 'AdminProjects.interior_designer'
        db.alter_column(u'admin_projects', 'interior_designer', self.gf('django.db.models.fields.CharField')(max_length=255))
        # Removing index on 'AdminProjects', fields ['interior_designer']
        db.delete_index(u'admin_projects', ['interior_designer_id'])


    def backwards(self, orm):
        # Adding index on 'AdminProjects', fields ['interior_designer']
        db.create_index(u'admin_projects', ['interior_designer_id'])

        # Adding index on 'AdminProjects', fields ['architect']
        db.create_index(u'admin_projects', ['architect_id'])

        # Adding index on 'AdminProjects', fields ['builder']
        db.create_index(u'admin_projects', ['builder_id'])

        # Adding model 'AdminConsultants'
        db.create_table(u'admin_consultants', (
            ('website', self.gf('django.db.models.fields.URLField')(default='', max_length=255, null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.IntegerField')()),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'main', ['AdminConsultants'])

        # Adding field 'AdminProjects.housing_type'
        db.add_column(u'admin_projects', 'housing_type',
                      self.gf('main.models.TagEditField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'AdminProjects.square_footage_range'
        db.add_column(u'admin_projects', 'square_footage_range',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AdminProjects.lot_size'
        db.add_column(u'admin_projects', 'lot_size',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AdminProjects.add_map'
        db.add_column(u'admin_projects', 'add_map',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'AdminProjects.city'
        db.add_column(u'admin_projects', 'city',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100),
                      keep_default=False)

        # Adding field 'AdminProjects.sales_to_date'
        db.add_column(u'admin_projects', 'sales_to_date',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AdminProjects.zip'
        db.add_column(u'admin_projects', 'zip',
                      self.gf('django.db.models.fields.CharField')(max_length=5, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AdminProjects.density'
        db.add_column(u'admin_projects', 'density',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AdminProjects.landscape_architect'
        db.add_column(u'admin_projects', 'landscape_architect',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='admin_landscape_architect_projects', null=True, to=orm['main.AdminConsultants'], blank=True),
                      keep_default=False)

        # Adding field 'AdminProjects.other_meta_words'
        db.add_column(u'admin_projects', 'other_meta_words',
                      self.gf('main.models.TagEditField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'AdminProjects.state'
        db.add_column(u'admin_projects', 'state',
                      self.gf('localflavor.us.models.USStateField')(default='', max_length=2),
                      keep_default=False)

        # Adding field 'AdminProjects.website'
        db.add_column(u'admin_projects', 'website',
                      self.gf('django.db.models.fields.URLField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AdminProjects.sales_start_date'
        db.add_column(u'admin_projects', 'sales_start_date',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AdminProjects.phone'
        db.add_column(u'admin_projects', 'phone',
                      self.gf('localflavor.us.models.PhoneNumberField')(max_length=20, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AdminProjects.address'
        db.add_column(u'admin_projects', 'address',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100),
                      keep_default=False)

        # Adding field 'AdminProjects.price_range'
        db.add_column(u'admin_projects', 'price_range',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'AdminProjects.consultants'
        db.delete_column(u'admin_projects', 'consultants')

        # Deleting field 'AdminProjects.location'
        db.delete_column(u'admin_projects', 'location')

        # Deleting field 'AdminProjects.profile'
        db.delete_column(u'admin_projects', 'profile')

        # Deleting field 'AdminProjects.meta_keywords'
        db.delete_column(u'admin_projects', 'meta_keywords')

        # Adding M2M table for field other_consultants on 'AdminProjects'
        m2m_table_name = db.shorten_name(u'admin_projects_other_consultants')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('adminprojects', models.ForeignKey(orm[u'main.adminprojects'], null=False)),
            ('adminconsultants', models.ForeignKey(orm[u'main.adminconsultants'], null=False))
        ))
        db.create_unique(m2m_table_name, ['adminprojects_id', 'adminconsultants_id'])


        # Renaming column for 'AdminProjects.builder' to match new field type.
        db.rename_column(u'admin_projects', 'builder', 'builder_id')
        # Changing field 'AdminProjects.builder'
        db.alter_column(u'admin_projects', 'builder_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.AdminConsultants']))

        # Changing field 'AdminProjects.project_date'
        db.alter_column(u'admin_projects', 'project_date', self.gf('django.db.models.fields.DateField')())

        # Renaming column for 'AdminProjects.architect' to match new field type.
        db.rename_column(u'admin_projects', 'architect', 'architect_id')
        # Changing field 'AdminProjects.architect'
        db.alter_column(u'admin_projects', 'architect_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.AdminConsultants']))

        # Renaming column for 'AdminProjects.interior_designer' to match new field type.
        db.rename_column(u'admin_projects', 'interior_designer', 'interior_designer_id')
        # Changing field 'AdminProjects.interior_designer'
        db.alter_column(u'admin_projects', 'interior_designer_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.AdminConsultants']))

    models = {
        u'main.adminimages': {
            'Meta': {'object_name': 'AdminImages', 'db_table': "u'admin_images'"},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_2': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_2': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'keywords': ('main.models.TagEditField', [], {'null': 'True', 'blank': 'True'}),
            'keywords_2': ('main.models.TagEditField', [], {'null': 'True', 'blank': 'True'}),
            'layout': ('django.db.models.fields.IntegerField', [], {'default': '1', 'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'admin_images_project'", 'to': u"orm['main.AdminProjects']"}),
            'time_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'main.adminprojects': {
            'Meta': {'object_name': 'AdminProjects', 'db_table': "u'admin_projects'"},
            'architect': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'builder': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'consultants': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interior_designer': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'location': ('django.db.models.fields.TextField', [], {'max_length': '255'}),
            'meta_keywords': ('main.models.TagEditField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'prod_site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'profile': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'project_date': ('django.db.models.fields.IntegerField', [], {'max_length': '6'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'site_admin_projects'", 'to': u"orm['main.Sites']"})
        },
        u'main.aliases': {
            'Meta': {'object_name': 'Aliases', 'db_table': "u'aliases'"},
            'alias': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'main.consultants': {
            'Meta': {'object_name': 'Consultants', 'db_table': "u'consultants'"},
            'consultants': ('django.db.models.fields.CharField', [], {'max_length': '8192', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.contact': {
            'Meta': {'object_name': 'Contact', 'db_table': "u'contact'"},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '75', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '75', 'blank': 'True'})
        },
        u'main.dblogactions': {
            'Meta': {'object_name': 'DbLogActions', 'db_table': "u'db_log_actions'"},
            'action_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sub_action_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'target_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'target_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.dblogcounters': {
            'Meta': {'object_name': 'DbLogCounters', 'db_table': "u'db_log_counters'"},
            'action_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'count': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'target_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.deletedimages': {
            'Meta': {'object_name': 'DeletedImages', 'db_table': "u'deleted_images'"},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'floorplan1': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'floorplan2': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'layout': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'meta': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            'meta_all': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sibling': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_plan': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.floorplan': {
            'Meta': {'object_name': 'Floorplan', 'db_table': "u'floorplan'"},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.idlimages': {
            'Meta': {'object_name': 'IdlImages', 'db_table': "u'idl_images'"},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'floorplan1': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'floorplan2': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'layout': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'meta': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            'meta_all': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sibling': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_plan': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'time_updated': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.images': {
            'Meta': {'object_name': 'Images', 'db_table': "u'images'"},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'floorplan1': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'floorplan2': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'layout': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'meta': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            'meta_all': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sibling': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_plan': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'time_updated': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.inbox': {
            'Meta': {'object_name': 'Inbox', 'db_table': "u'inbox'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.message': {
            'Meta': {'object_name': 'Message', 'db_table': "u'message'"},
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'min_access_level': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.permissions': {
            'Meta': {'object_name': 'Permissions', 'db_table': "u'permissions'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'perms1': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.profiles': {
            'Meta': {'object_name': 'Profiles', 'db_table': "u'profiles'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'profile': ('django.db.models.fields.CharField', [], {'max_length': '8192', 'blank': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.projects': {
            'Meta': {'object_name': 'Projects', 'db_table': "u'projects'"},
            'architect': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'builder': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'cardinal': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'historical_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interior_designer': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'length': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_words': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'project_date': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'project_type': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'time_updated': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.requests': {
            'Meta': {'object_name': 'Requests', 'db_table': "u'requests'"},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'image_site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'note': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'main.sites': {
            'Meta': {'object_name': 'Sites', 'db_table': "u'sites'"},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'site_identifier': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.tempattachment': {
            'Meta': {'object_name': 'TempAttachment'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'temp_hash': ('django.db.models.fields.CharField', [], {'max_length': "'255'"}),
            'upl': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        u'main.tracker': {
            'Meta': {'object_name': 'Tracker', 'db_table': "u'tracker'"},
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'landing_id': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.users': {
            'Meta': {'object_name': 'Users', 'db_table': "u'users'"},
            'access_level': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'company_name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hours_to_live': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_logged_in': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'password_md5': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sub_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'time_limited_start': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'times_logged_in': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'weeks_available': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.xml': {
            'Meta': {'object_name': 'Xml', 'db_table': "u'xml'"},
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True'}),
            'xml': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        }
    }

    complete_apps = ['main']