# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Aliases'
        db.create_table(u'aliases', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')()),
            ('user_id', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=1, blank=True)),
            ('alias', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('time_created', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('flags', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['Aliases'])

        # Adding model 'Consultants'
        db.create_table(u'consultants', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project_id', self.gf('django.db.models.fields.BigIntegerField')()),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('consultants', self.gf('django.db.models.fields.CharField')(max_length=8192, blank=True)),
        ))
        db.send_create_signal(u'main', ['Consultants'])

        # Adding model 'Contact'
        db.create_table(u'contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_id', self.gf('django.db.models.fields.IntegerField')()),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=75, blank=True)),
            ('website', self.gf('django.db.models.fields.CharField')(max_length=75, blank=True)),
        ))
        db.send_create_signal(u'main', ['Contact'])

        # Adding model 'IdlImages'
        db.create_table(u'idl_images', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image_id', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('project_id', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('layout', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('sibling', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('floorplan1', self.gf('django.db.models.fields.CharField')(max_length=8, blank=True)),
            ('floorplan2', self.gf('django.db.models.fields.CharField')(max_length=8, blank=True)),
            ('flags', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('site_plan', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('meta', self.gf('django.db.models.fields.CharField')(max_length=512, blank=True)),
            ('meta_all', self.gf('django.db.models.fields.CharField')(max_length=1024, blank=True)),
            ('time_updated', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['IdlImages'])

        # Adding model 'Floorplan'
        db.create_table(u'floorplan', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project_id', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('flags', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('index', self.gf('django.db.models.fields.CharField')(max_length=8, blank=True)),
        ))
        db.send_create_signal(u'main', ['Floorplan'])

        # Adding model 'Message'
        db.create_table(u'message', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('time_created', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('min_access_level', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('flags', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('subject', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('message', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'main', ['Message'])

        # Adding model 'Images'
        db.create_table(u'images', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image_id', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('project_id', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('layout', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('sibling', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('floorplan1', self.gf('django.db.models.fields.CharField')(max_length=8, blank=True)),
            ('floorplan2', self.gf('django.db.models.fields.CharField')(max_length=8, blank=True)),
            ('flags', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('site_plan', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('meta', self.gf('django.db.models.fields.CharField')(max_length=512, blank=True)),
            ('meta_all', self.gf('django.db.models.fields.CharField')(max_length=1024, blank=True)),
            ('time_updated', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['Images'])

        # Adding model 'Permissions'
        db.create_table(u'permissions', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('perms1', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['Permissions'])

        # Adding model 'Profiles'
        db.create_table(u'profiles', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project_id', self.gf('django.db.models.fields.BigIntegerField')()),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('profile', self.gf('django.db.models.fields.CharField')(max_length=8192, blank=True)),
        ))
        db.send_create_signal(u'main', ['Profiles'])

        # Adding model 'Tracker'
        db.create_table(u'tracker', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('landing_id', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
            ('user_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('time_created', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('flags', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['Tracker'])

        # Adding model 'Requests'
        db.create_table(u'requests', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('image_site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('image_id', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('username', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('note', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
            ('time_created', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['Requests'])

        # Adding model 'DbLogActions'
        db.create_table(u'db_log_actions', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('user_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('action_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('sub_action_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('target_text', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('target_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['DbLogActions'])

        # Adding model 'DbLogCounters'
        db.create_table(u'db_log_counters', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('action_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('target_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('count', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['DbLogCounters'])

        # Adding model 'DeletedImages'
        db.create_table(u'deleted_images', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image_id', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('project_id', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('layout', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('sibling', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('floorplan1', self.gf('django.db.models.fields.CharField')(max_length=8, blank=True)),
            ('floorplan2', self.gf('django.db.models.fields.CharField')(max_length=8, blank=True)),
            ('flags', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('site_plan', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('meta', self.gf('django.db.models.fields.CharField')(max_length=512, blank=True)),
            ('meta_all', self.gf('django.db.models.fields.CharField')(max_length=1024, blank=True)),
        ))
        db.send_create_signal(u'main', ['DeletedImages'])

        # Adding model 'Inbox'
        db.create_table(u'inbox', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('message_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
        ))
        db.send_create_signal(u'main', ['Inbox'])

        # Adding model 'Projects'
        db.create_table(u'projects', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project_id', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('architect', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('builder', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('cardinal', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
            ('project_date', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('historical_text', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('name', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('location', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('interior_designer', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('length', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('meta_words', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('flags', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('project_type', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('time_updated', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['Projects'])

        # Adding model 'Sites'
        db.create_table(u'sites', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('site_identifier', self.gf('django.db.models.fields.CharField')(max_length=256, blank=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1024, blank=True)),
            ('time_created', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('flags', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['Sites'])

        # Adding model 'Users'
        db.create_table(u'users', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('username', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('sub_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('flags', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('access_level', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('last_logged_in', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('times_logged_in', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('time_created', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('hours_to_live', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
            ('company_name', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('time_limited_start', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('weeks_available', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('password_md5', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal(u'main', ['Users'])

        # Adding model 'Xml'
        db.create_table(u'xml', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('site_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('project_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('flags', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('xml', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=1, blank=True)),
            ('time_created', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['Xml'])


    def backwards(self, orm):
        # Deleting model 'Aliases'
        db.delete_table(u'aliases')

        # Deleting model 'Consultants'
        db.delete_table(u'consultants')

        # Deleting model 'Contact'
        db.delete_table(u'contact')

        # Deleting model 'IdlImages'
        db.delete_table(u'idl_images')

        # Deleting model 'Floorplan'
        db.delete_table(u'floorplan')

        # Deleting model 'Message'
        db.delete_table(u'message')

        # Deleting model 'Images'
        db.delete_table(u'images')

        # Deleting model 'Permissions'
        db.delete_table(u'permissions')

        # Deleting model 'Profiles'
        db.delete_table(u'profiles')

        # Deleting model 'Tracker'
        db.delete_table(u'tracker')

        # Deleting model 'Requests'
        db.delete_table(u'requests')

        # Deleting model 'DbLogActions'
        db.delete_table(u'db_log_actions')

        # Deleting model 'DbLogCounters'
        db.delete_table(u'db_log_counters')

        # Deleting model 'DeletedImages'
        db.delete_table(u'deleted_images')

        # Deleting model 'Inbox'
        db.delete_table(u'inbox')

        # Deleting model 'Projects'
        db.delete_table(u'projects')

        # Deleting model 'Sites'
        db.delete_table(u'sites')

        # Deleting model 'Users'
        db.delete_table(u'users')

        # Deleting model 'Xml'
        db.delete_table(u'xml')


    models = {
        u'main.aliases': {
            'Meta': {'object_name': 'Aliases', 'db_table': "u'aliases'"},
            'alias': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'main.consultants': {
            'Meta': {'object_name': 'Consultants', 'db_table': "u'consultants'"},
            'consultants': ('django.db.models.fields.CharField', [], {'max_length': '8192', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.contact': {
            'Meta': {'object_name': 'Contact', 'db_table': "u'contact'"},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '75', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '75', 'blank': 'True'})
        },
        u'main.dblogactions': {
            'Meta': {'object_name': 'DbLogActions', 'db_table': "u'db_log_actions'"},
            'action_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sub_action_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'target_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'target_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.dblogcounters': {
            'Meta': {'object_name': 'DbLogCounters', 'db_table': "u'db_log_counters'"},
            'action_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'count': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'target_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.deletedimages': {
            'Meta': {'object_name': 'DeletedImages', 'db_table': "u'deleted_images'"},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'floorplan1': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'floorplan2': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'layout': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'meta': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            'meta_all': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sibling': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_plan': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.floorplan': {
            'Meta': {'object_name': 'Floorplan', 'db_table': "u'floorplan'"},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.idlimages': {
            'Meta': {'object_name': 'IdlImages', 'db_table': "u'idl_images'"},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'floorplan1': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'floorplan2': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'layout': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'meta': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            'meta_all': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sibling': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_plan': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'time_updated': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.images': {
            'Meta': {'object_name': 'Images', 'db_table': "u'images'"},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'floorplan1': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'floorplan2': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'layout': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'meta': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            'meta_all': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sibling': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_plan': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'time_updated': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.inbox': {
            'Meta': {'object_name': 'Inbox', 'db_table': "u'inbox'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.message': {
            'Meta': {'object_name': 'Message', 'db_table': "u'message'"},
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'min_access_level': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.permissions': {
            'Meta': {'object_name': 'Permissions', 'db_table': "u'permissions'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'perms1': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.profiles': {
            'Meta': {'object_name': 'Profiles', 'db_table': "u'profiles'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'profile': ('django.db.models.fields.CharField', [], {'max_length': '8192', 'blank': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.projects': {
            'Meta': {'object_name': 'Projects', 'db_table': "u'projects'"},
            'architect': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'builder': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'cardinal': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'historical_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interior_designer': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'length': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_words': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'project_date': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'project_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'project_type': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'time_updated': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.requests': {
            'Meta': {'object_name': 'Requests', 'db_table': "u'requests'"},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_id': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'image_site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'note': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'main.sites': {
            'Meta': {'object_name': 'Sites', 'db_table': "u'sites'"},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'site_identifier': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.tracker': {
            'Meta': {'object_name': 'Tracker', 'db_table': "u'tracker'"},
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'landing_id': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.users': {
            'Meta': {'object_name': 'Users', 'db_table': "u'users'"},
            'access_level': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'company_name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hours_to_live': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_logged_in': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'password_md5': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sub_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'time_limited_start': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'times_logged_in': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'weeks_available': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.xml': {
            'Meta': {'object_name': 'Xml', 'db_table': "u'xml'"},
            'flags': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'time_created': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True'}),
            'xml': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        }
    }

    complete_apps = ['main']