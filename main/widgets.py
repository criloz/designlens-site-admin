from django.contrib.admin import widgets as admin_widgets
from django import forms
import uuid
from django.utils.safestring import mark_safe
import json


class TagEditWidget(forms.Textarea):

    def __init__(self, attrs=None, taget_edit_attrs={}):
        super(TagEditWidget, self).__init__(attrs)
        self.taget_edit_attrs = taget_edit_attrs

    def render(self, name, value, attrs=None):

        try:
            value = json.loads(value)
        except:
            value = []

        html_class = "tag" + str(uuid.uuid4().fields[-1])[:5]
        autocomplete_name = html_class + "_autocomplete"
        autocomplete_json = []
        counter = 1

        for keyword in self.taget_edit_attrs.get("auto_complete", []):
            autocomplete_json.append({"id": counter, "value": keyword, "label": keyword})
            counter += 1
        html = list()

        if len(value):
            for item in value:
                html.append('<input type="text" name="%s[]" value="%s" class="%s"/>' % (name, item, html_class))
        else:
                html.append('<input type="text" name="%s[]" value="%s" class="%s"/>' % (name, "", html_class))

        if len(value):
            form_value = json.dumps(value)
        else:
            form_value = ""

        html.append('<input type="hidden" name="%s" value="%s" />' % (name, form_value))

        scripts = list()
        scripts.append("var %s = %s;" % (autocomplete_name, json.dumps(autocomplete_json)))
        scripts.append("jQuery('input.%s').tagedit({additionalListClass:'%s',\
                       autocompleteOptions:{autoFocus: true , source: %s}});"
                       % (html_class, html_class, autocomplete_name))

        scripts.append("""
                            $("form").submit(function(e){
                                var value_list = [];
                                $("ul.%s input").each(function(index){if($(this).val()!=""){value_list.push($(this).val())}})
                                if(value_list.length>0){
                                    jQuery("input[name='%s']").val(JSON.stringify(value_list));
                                }
                            })
                        """ % (html_class, name))

        script = '<script>%s</script>' % '\n'.join(scripts)
        html.append(script)
        return mark_safe('\n'.join(html))


class AdminTagEditWidget(admin_widgets.AdminTextareaWidget, TagEditWidget):
     def __init__(self, attrs=None, taget_edit_attrs=None):
        super(AdminTagEditWidget, self).__init__(attrs)
        self.taget_edit_attrs = taget_edit_attrs

class UrlWidget(forms.Textarea):
    def __init__(self, attrs=None):
        super(UrlWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        html = list()
        html.append("<a href='{0}'>{0}</a>".format(value))
        return mark_safe('\n'.join(html))

class AddConsultantWidget(forms.Textarea):

    def __init__(self, attrs=None, consultant_count=0):
        super(AddConsultantWidget, self).__init__(attrs)
        self.consultant_count = consultant_count

    def render(self, name, value, attrs=None):
        html = list()
        html.append("<input type='button' onclick='add_consultant_function();' value='Add Consultant' id='add_consultant'>")
        scripts = list()
        scripts.append("""
                String.prototype.format = function() {
        var str = this;
        var i = 0;
        var len = arguments.length;
        var matches = str.match(/{}/g);
        if( !matches || matches.length !== len ) {
            throw "wrong number of arguments";
        }
        while( i < len ) {
            str = str.replace(/{}/, arguments[i] );
            i++;
        }
        return str;
    };
            """)
        scripts.append(""" consultant_count = %i;""" % self.consultant_count)
        scripts.append(""" consultant_template = '<div class="grp-row grp-cells-1 consultant_{} "><div class="l-2c-fluid l-d-4"><div class="c-1"><label class="required" for="id_consultant_{}">Other {}</label></div><div class="c-2"><textarea cols="40" id="id_consultant_{}" name="consultant_{}" rows="10"></textarea></div></div></div>';""")
        scripts.append("""add_consultant_function = function(){
                    jQuery("#add_consultant").parents(".grp-row").before(jQuery(consultant_template.format(consultant_count+1, consultant_count+1, consultant_count+1, consultant_count+1, consultant_count+1)));
                    consultant_count = consultant_count + 1;
            };""")

        script = '<script>%s</script>' % '\n'.join(scripts)
        html.append(script)
        return mark_safe('\n'.join(html))
