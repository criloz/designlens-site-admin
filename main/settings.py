"""
Django settings for designlens2 project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_DIR = (os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%5i16^g@7qb_-&pn)7lx10(=%$_ein+_9fw(xq5_el%mu7!)^1'

# SECURITY WARNING: don't run with debug turned on in production!

ALLOWED_HOSTS = ["*"]


# Application definition

INSTALLED_APPS = (
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.auth',
    'django.contrib.staticfiles',

    #Non Standard
    'grappelli',
    'crispy_forms',
    'tinymce',
    'sorl.thumbnail',

    'django.contrib.admin',
    'south',

    #Custom
    'django.contrib.messages',
    'main',
    'gunicorn',

)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'main.urls'

WSGI_APPLICATION = 'main.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/


TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR,  'templates'),
)

STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, "static"),
)

STATIC_ROOT = 'staticfiles'
MEDIA_ROOT = 'media'
MEDIA_URL = '/media/'

STATIC_URL = '/static/'

TINYMCE_JS_ROOT = os.path.join("js/tinymce")
TINYMCE_JS_URL = os.path.join("js/tinymce/tinymce.min.js")

TINYMCE_DEFAULT_CONFIG = {

    'plugins': "advlist, anchor, charmap, code, contextmenu, fullscreen, image, link, "
               "lists, media, paste, preview, print, searchreplace, table, wordcount",
    'skin': "lightgray",
    'width': "900px",
    'height': "350px",

}
GRAPPELLI_ADMIN_TITLE = "DesignLens Admin Interface"



ENVIRONMENT = os.environ.get('ENVIRONMENT', "development")


try:
    DATABASES = {
        'default': {
            'ENGINE': os.environ['DB_ENGINE'],
            'NAME':  os.environ['DB_NAME'],
            'USER':  os.environ['DB_USER'],
            'PASSWORD': os.environ['DB_PASSWORD'],
            'HOST':  os.environ['DB_HOST'],
            'PORT':  os.environ['DB_PORT'],
        }
    }
except KeyError:
    try:
        from local_settings import *
    except ImportError:
        raise Exception("Configure a db using environmental variables or local_setting.py file")

import logging
from sorl.thumbnail.log import ThumbnailLogHandler


handler = ThumbnailLogHandler()
handler.setLevel(logging.ERROR)
logging.getLogger('sorl.thumbnail').addHandler(handler)
THUMBNAIL_DEBUG = True