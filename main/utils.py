__author__ = 'cristian'
import re
normal_image_regexp = re.compile("^([0-9]+)\.[jJ][pP][Ee]?[gG]$")
fp_image_regexp = re.compile("^[fF][pP]([0-9]+)\.jpe?g$")
sp_image_regexp = re.compile("^[Ss][pP]([0-9]+)\.jpe?g$")


def sort_images(regexp):

    def func(a, b):
        try:
            a_number = int(regexp.findall(a.file_name)[0])
        except IndexError:
            return -1

        try:
            b_number = int(regexp.findall(b.file_name)[0])
        except IndexError:
            return 1

        if a_number < b_number:
            return -1
        elif a_number > b_number:
            return 1
        else:
            return 0
    return func