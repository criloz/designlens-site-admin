from django.db import models
from localflavor.us import models as us_models
from django.utils.encoding import python_2_unicode_compatible
from tinymce import models as tinymce_models
from south.modelsinspector import add_introspection_rules
from django.contrib.admin import widgets as admin_widgets
from .widgets import AdminTagEditWidget, TagEditWidget
from django.core.validators import URLValidator

add_introspection_rules([], ["^localflavor\.us\.models\.USStateField"])
add_introspection_rules([], ["^localflavor\.us\.models\.PhoneNumberField"])
add_introspection_rules([], ["^localflavor\.us\.models\.USPostalCodeField"])
add_introspection_rules([], ["^tinymce\.models\.tinymce_models\.HTMLField"])
add_introspection_rules([], ["^main\.models\.TagEditField"])


def nested_pairs2dict(pairs):
    d = {}
    for k, v in pairs:
        if isinstance(v, list):  # assumes v is also list of pairs
            v = nested_pairs2dict(v)
        d[k] = v
    return d


class TagEditField(models.TextField):
    """
        TagEditField
    """
    def __init__(self, **kwargs):
        self.auto_complete = kwargs.pop("auto_complete", [])
        return super(TagEditField, self).__init__(**kwargs)

    def formfield(self, **kwargs):
        defaults = {'widget': TagEditWidget(taget_edit_attrs={"auto_complete": self.auto_complete})}
        defaults.update(kwargs)

        # As an ugly hack, we override the admin widget
        if defaults['widget'] == admin_widgets.AdminTextareaWidget:
            defaults['widget'] = AdminTagEditWidget(taget_edit_attrs={"auto_complete": self.auto_complete})
        return super(TagEditField, self).formfield(**defaults)


class Aliases(models.Model):
    site_id = models.IntegerField()
    user_id = models.IntegerField()
    name = models.CharField(max_length=64, blank=True)
    status = models.CharField(max_length=1, blank=True)
    alias = models.CharField(max_length=64, blank=True)
    time_created = models.DateField(null=True, blank=True)
    flags = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = u'aliases'


class Consultants(models.Model):
    project_id = models.BigIntegerField()
    site_id = models.IntegerField(null=True, blank=True)
    consultants = models.CharField(max_length=8192, blank=True)

    class Meta:
        db_table = u'consultants'


class Contact(models.Model):
    user_id = models.IntegerField()
    email = models.CharField(max_length=75, blank=True)
    website = models.CharField(max_length=75, blank=True)

    class Meta:
        db_table = u'contact'


class IdlImages(models.Model):

    image_id = models.BigIntegerField(null=True, blank=True)
    site_id = models.IntegerField(null=True, blank=True)
    description = models.TextField(blank=True)
    project_id = models.BigIntegerField(null=True, blank=True)
    layout = models.IntegerField(null=True, blank=True)
    sibling = models.BigIntegerField(null=True, blank=True)
    floorplan1 = models.CharField(max_length=8, blank=True)
    floorplan2 = models.CharField(max_length=8, blank=True)
    flags = models.IntegerField(null=True, blank=True)
    site_plan = models.IntegerField(null=True, blank=True)
    meta = models.CharField(max_length=512, blank=True)
    meta_all = models.CharField(max_length=1024, blank=True)
    time_updated = models.DateTimeField(null=True, blank=True)
    class Meta:
        db_table = u'idl_images'


class Floorplan(models.Model):

    project_id = models.BigIntegerField(null=True, blank=True)
    site_id = models.IntegerField(null=True, blank=True)
    flags = models.IntegerField(null=True, blank=True)
    description = models.TextField(blank=True)
    index = models.CharField(max_length=8, blank=True)

    class Meta:
        db_table = u'floorplan'


class Message(models.Model):
    site_id = models.IntegerField(null=True, blank=True)
    time_created = models.DateField(null=True, blank=True)
    min_access_level = models.IntegerField(null=True, blank=True)
    flags = models.IntegerField(null=True, blank=True)
    subject = models.TextField(blank=True)
    message = models.TextField(blank=True)

    class Meta:
        db_table = u'message'


class Images(models.Model):
    image_id = models.BigIntegerField(null=True, blank=True)
    site_id = models.IntegerField(null=True, blank=True)
    description = models.TextField(blank=True)
    project_id = models.BigIntegerField(null=True, blank=True)
    layout = models.IntegerField(null=True, blank=True)
    sibling = models.BigIntegerField(null=True, blank=True)
    floorplan1 = models.CharField(max_length=8, blank=True)
    floorplan2 = models.CharField(max_length=8, blank=True)
    flags = models.IntegerField(null=True, blank=True)
    site_plan = models.IntegerField(null=True, blank=True)
    meta = models.CharField(max_length=512, blank=True)
    meta_all = models.CharField(max_length=1024, blank=True)
    time_updated = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = u'images'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.flags = 1
        return super(Images, self).save(force_insert, force_update, using, update_fields)


class Permissions(models.Model):
    user_id = models.IntegerField(null=True, blank=True)
    perms1 = models.BigIntegerField(null=True, blank=True)

    class Meta:
        db_table = u'permissions'


class Profiles(models.Model):
    project_id = models.BigIntegerField()
    site_id = models.IntegerField(null=True, blank=True)
    profile = models.CharField(max_length=8192, blank=True)

    class Meta:
        db_table = u'profiles'


class Tracker(models.Model):
    site_id = models.IntegerField(null=True, blank=True)
    landing_id = models.CharField(max_length=32, blank=True)
    user_id = models.IntegerField(null=True, blank=True)
    time_created = models.DateField(null=True, blank=True)
    flags = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = u'tracker'


class Requests(models.Model):
    site_id = models.IntegerField(null=True, blank=True)
    image_site_id = models.IntegerField(null=True, blank=True)
    image_id = models.BigIntegerField(null=True, blank=True)
    type = models.CharField(max_length=64, blank=True)
    username = models.CharField(max_length=100, blank=True)
    email = models.CharField(max_length=255, blank=True)
    note = models.TextField(blank=True)
    status = models.CharField(max_length=32, blank=True)
    time_created = models.DateField(null=True, blank=True)

    class Meta:
        db_table = u'requests'


class DbLogActions(models.Model):
    site_id = models.IntegerField(null=True, blank=True)
    user_id = models.IntegerField(null=True, blank=True)
    action_id = models.IntegerField(null=True, blank=True)
    sub_action_id = models.IntegerField(null=True, blank=True)
    target_text = models.TextField(blank=True)
    target_id = models.IntegerField(null=True, blank=True)
    timestamp = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = u'db_log_actions'


class DbLogCounters(models.Model):
    site_id = models.IntegerField(null=True, blank=True)
    action_id = models.IntegerField(null=True, blank=True)
    target_id = models.IntegerField(null=True, blank=True)
    count = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = u'db_log_counters'


class DeletedImages(models.Model):

    image_id = models.BigIntegerField(null=True, blank=True)
    site_id = models.IntegerField(null=True, blank=True)
    description = models.TextField(blank=True)
    project_id = models.BigIntegerField(null=True, blank=True)
    layout = models.IntegerField(null=True, blank=True)
    sibling = models.BigIntegerField(null=True, blank=True)
    floorplan1 = models.CharField(max_length=8, blank=True)
    floorplan2 = models.CharField(max_length=8, blank=True)
    flags = models.IntegerField(null=True, blank=True)
    site_plan = models.IntegerField(null=True, blank=True)
    meta = models.CharField(max_length=512, blank=True)
    meta_all = models.CharField(max_length=1024, blank=True)

    class Meta:
        db_table = u'deleted_images'


class Inbox(models.Model):
    user_id = models.IntegerField(null=True, blank=True)
    site_id = models.IntegerField(null=True, blank=True)
    message_id = models.IntegerField(null=True, blank=True)
    status = models.CharField(max_length=32, blank=True)

    class Meta:
        db_table = u'inbox'


class Projects(models.Model):
    project_id = models.BigIntegerField(null=True, blank=True)
    site_id = models.IntegerField(null=True, blank=True)
    architect = models.TextField(blank=True)
    builder = models.TextField(blank=True)
    cardinal = models.BigIntegerField(null=True, blank=True)
    project_date = models.IntegerField(null=True, blank=True)
    historical_text = models.TextField(blank=True)
    name = models.TextField(blank=True)
    location = models.TextField(blank=True)
    interior_designer = models.TextField(blank=True)
    length = models.IntegerField(null=True, blank=True)
    meta_words = models.TextField(blank=True)
    flags = models.IntegerField(null=True, blank=True)
    project_type = models.IntegerField(null=True, blank=True)
    time_updated = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = u'projects'


@python_2_unicode_compatible
class Sites(models.Model):
    site_identifier = models.CharField(max_length=256, blank=True)
    description = models.CharField(max_length=1024, blank=True)
    time_created = models.DateField(null=True, blank=True)
    flags = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = u'sites'
        verbose_name_plural = "Sites"
        verbose_name = "Site"

    def __str__(self):
        return self.site_identifier


class Users(models.Model):
    class Meta:
        db_table = "users"
        app_label = 'subscribers'
        verbose_name_plural = "Users"
        verbose_name = "User"

    site_id = models.IntegerField(null=True, blank=True, default=2)
    username = models.CharField(max_length=100, blank=True)
    password = models.CharField(max_length=100, blank=True)
    sub_date = models.DateField(null=True, blank=True)
    flags = models.IntegerField(null=True, blank=True, default=0)
    access_level = models.IntegerField(null=True, blank=True, default=227)
    last_logged_in = models.DateField(null=True, blank=True)
    times_logged_in = models.IntegerField(null=True, blank=True)
    time_created = models.DateTimeField(null=True, blank=True)
    hours_to_live = models.IntegerField(null=True, blank=True)
    phone_number = models.CharField(max_length=32, blank=True)
    company_name = models.CharField(max_length=64, blank=True)
    time_limited_start = models.DateTimeField(null=True, blank=True)
    weeks_available = models.IntegerField(null=True, blank=True)
    password_md5 = models.CharField(max_length=100, blank=True)


class Xml(models.Model):
    site_id = models.IntegerField(null=True, blank=True)
    project_id = models.IntegerField(null=True, blank=True)
    flags = models.IntegerField(null=True, blank=True)
    xml = models.TextField(blank=True)
    type = models.CharField(max_length=1, blank=True)
    time_created = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = u'xml'


"""
    ==================================================================================
                            Admin site
    ==================================================================================
"""

PROJECT_FLAG_CHOICES = ((0, "0 - Regular project without site plan"),
                        (1, "1 - Regular project with site plan"),
                        (2, "2 - Historical Neighborhood"),
                        (4, "4 - Master Planned Communities without site plan"),
                        (5, "5 - Master Planned Communities with site plan"),
                        (8, "8 - Other Special Features"))



def image_list():
    pass


@python_2_unicode_compatible
class AdminProjects(models.Model):

    name = models.CharField(max_length=255, default="", blank=True)
    project_date = models.CharField(max_length=255, default="", blank=True)
    prod_site_id = models.IntegerField(null=True, editable=False)
    path = models.CharField(max_length=255, null=True, editable=False)
    architect = models.CharField(max_length=255, default="", blank=True)
    builder = models.CharField(max_length=255, default="", blank=True)
    interior_designer = models.CharField(max_length=255, default="", blank=True)
    flags = models.IntegerField(choices=PROJECT_FLAG_CHOICES)

    consultants = models.TextField(default="", blank=True)
    site = models.ForeignKey(Sites, editable=False, default=lambda: Sites.objects.get(id=2),
                             related_name="site_admin_projects")
    location = models.CharField(max_length=255, default="", blank=True)
    profile = models.TextField(blank=True, null=True)
    meta_keywords = TagEditField(null=True, blank=True, )
    cardinal = models.CharField(max_length=255, default="", blank=True, null=True)

    class Meta:
        db_table = u'admin_projects'
        verbose_name_plural = "Projects"
        verbose_name = "Project"

    def __str__(self):
        return self.name + "(project)"


HORIZONTAL = 1
VERTICAL_RIGHT_TEXT = 2
DOUBLE_HORIZONTAL_RIGHT = 3
DOUBLE_VERTICAL = 4
DOUBLE_HORIZONTAL_LEFT = 5
VERTICAL_LEFT_TEXT = 6
VERTICAL_CENTERED_TEXT = 7

LAYOUT_CHOICES = (
    (HORIZONTAL, 'Horizontal'),
    (VERTICAL_RIGHT_TEXT, 'Vertical, text on the right'),
    (DOUBLE_HORIZONTAL_RIGHT, 'Double horizontal'),
    (DOUBLE_VERTICAL, 'Double vertical'),
    (DOUBLE_HORIZONTAL_LEFT, 'Double, horizontal + vertical'),
    (VERTICAL_LEFT_TEXT, "Vertical, text on the left"),
    (VERTICAL_CENTERED_TEXT, "Vertical, centered")
)

LAYOUT_CHOICES_DICT = nested_pairs2dict(LAYOUT_CHOICES)

NORMAL_IMAGE = 1
HOMEPAGE_IMAGE = 2
FLOOR_PLAN_IMAGE = 3
SITE_PLAN_IMAGE = 4

IMAGE_TYPES_CHOICES = (
    (NORMAL_IMAGE, 'Normal'),
    (HOMEPAGE_IMAGE, 'Homepage image'),
    (FLOOR_PLAN_IMAGE, 'Floor plan'),
    (SITE_PLAN_IMAGE, 'Site plan'),
)


class TempAttachment(models.Model):
    temp_hash = models.CharField(max_length='255')
    upl = models.ImageField(upload_to='uploads')
    file_name = models.CharField(max_length=128)
    type = models.IntegerField(default=None, null=True, blank=True)


class AdminImages(models.Model):

    description = models.TextField(null=True, blank=True)
    description_2 = models.TextField(null=True, blank=True)
    project = models.ForeignKey(AdminProjects, related_name="admin_images_project")

    layout = models.IntegerField(choices=LAYOUT_CHOICES, null=True, blank=True, default=HORIZONTAL)
    type = models.IntegerField(default=NORMAL_IMAGE, choices=IMAGE_TYPES_CHOICES)

    keywords = TagEditField(null=True, blank=True)
    keywords_2 = TagEditField(null=True, blank=True)

    image = models.ImageField(upload_to="%Y/%m/%d")
    image_2 = models.ImageField(null=True, blank=True, upload_to="%Y/%m/%d")
    file_name = models.CharField(max_length=255)
    file_name_2 = models.CharField(max_length=255)
    floor_plan = models.IntegerField(null=True, blank=True)
    site_plan = models.IntegerField(null=True, blank=True)
    floor_plan_2 = models.IntegerField(null=True, blank=True)
    site_plan_2 = models.IntegerField(null=True, blank=True)

    time_updated = models.DateTimeField(auto_now=True)
    position = models.IntegerField(default=0, editable=False)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if self.type == HOMEPAGE_IMAGE:
            # search for others homepage images and downgrade to normal images
            for image in AdminImages.objects.filter(project=self.project, type=HOMEPAGE_IMAGE):
                image.type = NORMAL_IMAGE
                image.save()

        return super(AdminImages, self).save(force_insert, force_update, using, update_fields)

    @property
    def is_horizontal(self):
        return (self.layout in [HORIZONTAL, DOUBLE_HORIZONTAL_LEFT, DOUBLE_HORIZONTAL_RIGHT]) and self.image_2

    @property
    def is_double(self):
        return (self.layout in [DOUBLE_VERTICAL, DOUBLE_HORIZONTAL_LEFT, DOUBLE_HORIZONTAL_RIGHT]) and self.image_2

    @property
    def is_homepage(self):
        return self.type == HOMEPAGE_IMAGE

    class Meta:
        db_table = u'admin_images'
        verbose_name_plural = "Images"
        verbose_name = "Image"
