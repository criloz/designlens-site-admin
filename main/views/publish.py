from django.http import HttpResponse
from django.conf import settings
from .admin import AdminView
from django.shortcuts import get_object_or_404
from ..models import AdminImages, AdminProjects, Projects, Profiles, Consultants,\
    Images, HORIZONTAL, VERTICAL_RIGHT_TEXT, DOUBLE_HORIZONTAL_RIGHT, DOUBLE_VERTICAL, \
    DOUBLE_HORIZONTAL_LEFT, VERTICAL_LEFT_TEXT, VERTICAL_CENTERED_TEXT, NORMAL_IMAGE, SITE_PLAN_IMAGE, \
    FLOOR_PLAN_IMAGE, HOMEPAGE_IMAGE
from django.http import HttpResponseRedirect
import json
from django.core.exceptions import ObjectDoesNotExist
import shutil
from sorl.thumbnail import get_thumbnail
import os
import re
HOMEPAGE_IMAGE_LAYOUT = 30

from ..utils import *
from PIL import Image
from django.http import Http404


def make_image(image, layout, name, project_path, thumbnail=False, index=0):
    lower_name = name.lower()
    image = str(image)
    if not image.strip():
        return
    im_path = os.path.join(settings.BASE_DIR, settings.MEDIA_ROOT,  image.strip("/"))
    im_dest = os.path.join(project_path, "{0}".format(lower_name))
    shutil.copy(im_path, im_dest)
    print(os.popen("chown rtanaka:designlens {0}".format(im_dest)).read())
    print(os.popen("chmod 755 {0}".format(im_dest)).read())

    if thumbnail:
        img = Image.open(im_path)
        width, height = img.size
        del img
        proportion = 100.0/height
        new_width = int(width*proportion)
        thumb = get_thumbnail(image, "%sx%s" % (new_width, 100), crop='center', quality=99)
        thumb_path = os.path.join(settings.BASE_DIR,  thumb.url.strip("/"))
        thumb_dest = os.path.join(project_path, "thumb_{0}.jpg".format(index))
        shutil.copy(thumb_path, thumb_dest)
        print(os.popen("chown rtanaka:designlens {0}".format(thumb_dest)).read())
        print(os.popen("chmod 755 {0}".format(thumb_dest)).read())


def make_public(project_id, old_id=None):
    old_id = str(old_id)

    project = get_object_or_404(AdminProjects, id=project_id)

    if project.prod_site_id is not None:
        return HttpResponse("Project already published", status=404)

    #get the project id
    month = project.project_date[-2:]
    year = project.project_date[:4]

    directory = "{site}/{year}/{month}/".format(site=str(project.site.pk), year=str(year), month=str(month))
    directory = os.path.join(settings.SHARED_ROOT, directory)

    if not os.path.exists(directory):
        os.makedirs(directory)

    # change group
    print(os.popen("chown rtanaka:designlens {0}".format(directory)).read())
    print(os.popen("chmod 755 {0}".format(directory)).read())
    old_month = old_id[4:6]
    old_year = old_id[:4]
    old_db_id = old_id[6:]

    if old_month == month and old_year == year and (int(old_db_id) <= 9):
        new_project_id = int(old_db_id)
        project_dir = os.path.join(directory, str(new_project_id))

        try:
            os.makedirs(project_dir)
        except OSError:
            pass

        project.path = project_dir
        project.prod_site_id = int("{year}{month}{new_id}".format(new_id=str(new_project_id),
                                                                  year=str(year), month=str(month)))
    else:
        paths = []
        #list directories
        for path in os.listdir(directory):
            try:
                int_path = int(path)
                paths.append(int_path)
            except (ValueError, TypeError), e:
                continue
        possibles_directories = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        available_directories = []

        for i in possibles_directories:
            if i not in paths:
                available_directories.append(i)

        if not available_directories:
            raise Http404("I cant publish this project, Error: Max project by month reached ")

        value_id = available_directories[0]
        new_project_id = value_id
        project_dir = os.path.join(directory, str(new_project_id))
        try:
            os.makedirs(project_dir)
        except OSError:
            pass
        project.path = project_dir
        project.prod_site_id = int("{year}{month}{new_id}".format(new_id=str(new_project_id),
                                                                  year=str(year), month=str(month)))
    project.save()
    #save new project
    prod_project = Projects()
    prod_project.site_id = project.site.pk
    prod_project.name = project.name
    prod_project.project_id = project.prod_site_id
    prod_project.architect = project.architect
    prod_project.builder = project.builder
    prod_project.interior_designer = project.interior_designer
    prod_project.project_date = int("{year}{month}".format(year=str(year), month=str(month)))

    #save metawords
    try:
        meta_words = json.loads(project.meta_keywords)
    except:
        meta_words = []

    meta_words.append(project.name)
    meta_words.append(project.interior_designer)
    meta_words.append(project.builder)
    meta_words.append(project.architect)
    meta_words.append(project.location)

    prod_project.meta_words = " ".join(meta_words)
    prod_project.location = project.location
    prod_project.project_type = 1
    prod_project.save()

    new_profile = Profiles()
    new_profile.project_id = prod_project.project_id
    new_profile.site_id = prod_project.site_id
    new_profile.profile = project.profile
    new_profile.save()

    #save consultants
    new_consultant = Consultants()
    new_consultant.project_id = prod_project.project_id
    new_consultant.site_id = prod_project.site_id
    new_consultant.consultants = project.consultants
    new_consultant.save()

    #images
    images = AdminImages.objects.filter(project=project).all()
    normal_list = list()
    site_plan_list = list()
    floor_plan_list = list()

    for db_image in images:

        if db_image.type == FLOOR_PLAN_IMAGE:
            match = fp_image_regexp.findall(db_image.file_name)
            floor_plan_list.append((int(match[0]), db_image))

        elif db_image.type == SITE_PLAN_IMAGE:
            match = sp_image_regexp.findall(db_image.file_name)
            site_plan_list.append((int(match[0]), db_image))

        else:
            match = normal_image_regexp.findall(db_image.file_name)
            normal_list.append((int(match[0]), db_image))

    floor_plan_list.sort()
    site_plan_list.sort()
    normal_list.sort()

    #save floor plans
    for floor_plan_tuple in floor_plan_list:
        floor_plan = floor_plan_tuple[1]
        make_image(floor_plan.image, floor_plan.layout, floor_plan.file_name, project.path, thumbnail=False)

    for site_plan_tuple in site_plan_list:
        site_plan = site_plan_tuple[1]
        make_image(site_plan.image, site_plan.layout, site_plan.file_name, project.path, thumbnail=False)

    img_counter = 1
    for normal_image_tuple in normal_list:
        normal_image = normal_image_tuple[1]
        if normal_image.layout in [DOUBLE_HORIZONTAL_RIGHT, DOUBLE_VERTICAL,  DOUBLE_HORIZONTAL_LEFT]:

            new_image = Images()
            new_image.image_id = int(str(prod_project.project_id) + str(img_counter))
            if normal_image.file_name == project.cardinal:
                prod_project.cardinal = int(new_image.image_id)
            new_image.site_id = prod_project.site_id
            new_image.description = normal_image.description
            new_image.project_id = prod_project.project_id
            new_image.layout = normal_image.layout
            new_image.floorplan1 = str(normal_image.floor_plan) if normal_image.floor_plan else ""
            new_image.site_plan = str(normal_image.site_plan) if normal_image.site_plan else None

            try:
                _meta = json.loads(normal_image.keywords)
            except:
                _meta = []

            new_image.meta = " ".join(_meta)

            new_image.save()
            make_image(normal_image.image, normal_image.layout, normal_image.file_name, project.path, thumbnail=True,
                       index=img_counter)

            img_counter += 1

            new_image_2 = Images()
            new_image_2.image_id = int(str(prod_project.project_id) + str(img_counter))
            if normal_image.file_name_2 == project.cardinal:
                prod_project.cardinal = int(new_image_2.image_id)
            new_image_2.site_id = prod_project.site_id
            new_image_2.description = normal_image.description_2
            new_image_2.project_id = prod_project.project_id
            new_image_2.layout = normal_image.layout
            new_image_2.floorplan1 = str(normal_image.floor_plan_2) if normal_image.floor_plan_2 else ""
            new_image_2.site_plan = normal_image.site_plan_2 if normal_image.site_plan_2 else None

            try:
                _meta = json.loads(normal_image.keywords_2)
            except:
                _meta = []

            new_image_2.meta = " ".join(_meta)
            new_image_2.save()
            new_image_2.sibling = new_image.image_id
            new_image_2.save()
            new_image.sibling = new_image_2.image_id
            new_image.save()

            make_image(normal_image.image_2, normal_image.layout, normal_image.file_name_2, project.path,
                       thumbnail=True, index=img_counter)
            img_counter += 1

            continue

        else:
            new_image = Images()
            new_image.image_id = int(str(prod_project.project_id) + str(img_counter))
            new_image.site_id = prod_project.site_id
            new_image.description = normal_image.description
            new_image.project_id = prod_project.project_id
            new_image.layout = normal_image.layout
            new_image.floorplan1 = str(normal_image.floor_plan) if normal_image.floor_plan else ""
            new_image.site_plan = normal_image.site_plan if normal_image.site_plan else None
            if normal_image.file_name == project.cardinal:
                prod_project.cardinal = int(new_image.image_id)

            try:
                _meta = json.loads(normal_image.keywords)
            except:
                _meta = []
            new_image.meta = " ".join(_meta)
            new_image.save()
            make_image(normal_image.image, normal_image.layout, normal_image.file_name, project.path,
                       thumbnail=True, index=img_counter)
            img_counter += 1
            continue

    prod_project.length = img_counter-1
    prod_project.flags = project.flags
    prod_project.save()


def un_publish(project_id):
    project = get_object_or_404(AdminProjects, id=project_id)

    if project.prod_site_id is None:
        return HttpResponse("Project is not published", status=404)

    #delete prod project
    try:
        prod_project = Projects.objects.filter(project_id=project.prod_site_id, site_id=project.site.pk)
        prod_project.delete()
    except ObjectDoesNotExist:
        pass

    #delete prod profile
    try:
        prod_profile = Profiles.objects.filter(site_id=project.site.pk, project_id=project.prod_site_id)
        prod_profile.delete()
    except ObjectDoesNotExist:
        pass

    #delete consultants
    Consultants.objects.filter(site_id=project.site.pk, project_id=project.prod_site_id).delete()
    #delete images
    Images.objects.filter(site_id=project.site.pk, project_id=project.prod_site_id).delete()
    #delete path
    try:
        shutil.rmtree(project.path)
    except:
        pass
    project.prod_site_id = None
    project.path = None
    project.save()


def call_search_script():
    if hasattr(settings, "SEARCH_SCRIPT") and settings.SEARCH_SCRIPT:
        print os.popen('python2.7 "%s"' % settings.SEARCH_SCRIPT).read()


class AdminPublishView(AdminView):

    def post(self, request, *args, **kwargs):
        project_id = self.args[0]
        make_public(project_id)
        call_search_script()
        return HttpResponseRedirect(redirect_to='/admin/main/adminprojects/%s/' % str(project_id))


class AdminUnPublishView(AdminView):

    def post(self, request, *args, **kwargs):
        project_id = self.args[0]
        un_publish(project_id)
        call_search_script()
        return HttpResponseRedirect(redirect_to='/admin/main/adminprojects/%s/' % str(project_id))


class AdminRePublishView(AdminView):

    def post(self, request, *args, **kwargs):
        project_id = self.args[0]
        project = get_object_or_404(AdminProjects, id=project_id)
        prod_site_id = project.prod_site_id
        un_publish(project_id)
        make_public(project_id, old_id=prod_site_id)
        call_search_script()
        return HttpResponseRedirect(redirect_to='/admin/main/adminprojects/%s/' % str(project_id))
