__author__ = 'cristian'
from ..models import HOMEPAGE_IMAGE, NORMAL_IMAGE, SITE_PLAN_IMAGE, FLOOR_PLAN_IMAGE, HORIZONTAL, DOUBLE_HORIZONTAL_LEFT
from .admin import AdminView
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, DeleteView
from ..models import AdminImages, AdminProjects, TempAttachment
from django.http import QueryDict
from django.http import HttpResponse
import uuid
import json
from django import forms
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from sorl.thumbnail import get_thumbnail
from ..utils import *

__all__ = ["AdminSaveImageAjaxView", "AdminImagesDeleteView",
           "AdminImagesListView", "AdminImagesMultiUploadsView",
           "AdminImagesMultiSaveView"]


class AttachmentForm(forms.ModelForm):
    class Meta:
        model = TempAttachment


def to_int(string):

    try:
        return int(string)
    except:
        return None


class AdminSaveImageAjaxView(AdminView):

    def post(self, request, *args, **kwargs):
        data = request.POST.get("data", '{}')
        parsed_data = json.loads(data)
        image = get_object_or_404(AdminImages, pk=self.args[0])
        is_homepage = parsed_data.pop("is_homepage", False)
        delete = parsed_data.pop("delete", False)
        create = parsed_data.pop("create", False)
        new_image = None
        if create:
            image.image_2 = ""
            image.description_2 = ""
            image.file_name_2 = ""
            image.keywords_2 = ""
            image.floor_plan_2 = None
            image.site_plan_2 = None
            new_image = AdminImages()
            new_image.image = create["image"]
            new_image.description = create["description"]
            new_image.file_name = create["file_name"]
            new_image.keywords = create["keywords"]
            new_image.project = image.project
            new_image.layout = parsed_data["layout"]
            new_image.type = NORMAL_IMAGE
            new_image.site_plan = to_int(parsed_data["site_plan"])
            new_image.floor_plan = to_int(parsed_data["floor_plan"])
            new_image.save()

        if delete:
            try:
                delete_image = AdminImages.objects.get(pk=delete["pk"])
                delete_image.delete()
            except ObjectDoesNotExist:
                pass
            image.image_2 = delete["image"]
            image.description_2 = delete["description"]
            image.file_name_2 = delete["file_name"]
            image.keywords_2 = delete["keywords"]
            image.site_plan_2 = to_int(delete["site_plan"])
            image.floor_plan_2 = to_int(delete["floor_plan"])

        if is_homepage:
            image.type = HOMEPAGE_IMAGE

        for key, value in parsed_data.items():
            try:
                value = value.strip()
            except:
                pass

            if value not in ("", "None", None):
                if key.startswith("keywords"):
                    value = json.dumps(value.strip().split())
                setattr(image, key, value)
            elif value == "" and (key in ["description", "description_2"]):
                setattr(image, key, value)

        image.save()
        ret = {"success": "ok"}
        if new_image:
            ret["new_image_pk"] = new_image.pk
            thumb = get_thumbnail(new_image.image, "100x100", crop='center', quality=99)
            ret["new_image_thumbnail"] = thumb.url

        return HttpResponse(json.dumps(ret), content_type="application/json")


class AdminImagesDeleteView(DeleteView, AdminView):
    model = AdminImages


class AdminImagesMultiSaveView(TemplateView, AdminView):
    template_name = 'multi-save.html'

    def post(self, request, *args, **kwargs):

        data = request.POST.get("data", '{}')
        parsed_data = json.loads(data)

        #save normal_images
        for obj in parsed_data["normal_images"]:
            image = AdminImages()
            project = get_object_or_404(AdminProjects, id=obj["project"])
            image.project = project
            image.layout = HORIZONTAL
            image.type = NORMAL_IMAGE
            image.image = obj["src"]
            image.file_name = obj["file_name"]
            image.save()

        for obj in parsed_data["site_plan_images"]:
            image = AdminImages()
            project = get_object_or_404(AdminProjects, id=obj["project"])
            image.project = project
            image.layout = HORIZONTAL
            image.type = SITE_PLAN_IMAGE
            image.image = obj["src"]
            image.file_name = obj["file_name"]
            image.save()

        for obj in parsed_data["floor_plan_images"]:
            image = AdminImages()
            project = get_object_or_404(AdminProjects, id=obj["project"])
            image.project = project
            image.layout = HORIZONTAL
            image.type = FLOOR_PLAN_IMAGE
            image.image = obj["src"]
            image.file_name = obj["file_name"]
            image.save()

        for obj in parsed_data["home_page_images"]:
            image = AdminImages()
            project = get_object_or_404(AdminProjects, id=obj["project"])
            image.project = project
            image.layout = HORIZONTAL
            image.type = HOMEPAGE_IMAGE
            image.image = obj["src"]
            image.file_name = obj["file_name"]
            image.save()
            break

        for obj in parsed_data["double_images"]:
            image = AdminImages()
            project = get_object_or_404(AdminProjects, id=obj["image1"]["project"])
            image.project = project
            image.layout = DOUBLE_HORIZONTAL_LEFT
            image.type = NORMAL_IMAGE
            image.image = obj["image1"]["src"]
            image.image_2 = obj["image2"]["src"]
            image.file_name = obj["image1"]["file_name"]
            image.file_name_2 = obj["image2"]["file_name"]
            image.save()

        return HttpResponse(json.dumps({"success": "ok"}), content_type="application/json")

    def get_queryset(self):
        _hash = self.request.GET.get("hash")
        return TempAttachment.objects.filter(temp_hash=_hash)

    def get_context_data(self, **kwargs):
        context = super(AdminImagesMultiSaveView, self).get_context_data(**kwargs)
        context['project_id'] = self.args[0]
        context['image_list'] = self.get_queryset()
        return context


class AdminImagesMultiUploadsView(TemplateView, AdminView):
    template_name = 'multi-uploads.html'

    def get_context_data(self, **kwargs):
        context = super(AdminImagesMultiUploadsView, self).get_context_data(**kwargs)
        context['hash'] = str(uuid.uuid4())
        project = get_object_or_404(AdminProjects, id=self.args[0])
        context['project_id'] = project.pk
        return context

    def post(self, request, *args, **kwargs):
        # Ajax POST for file uploads
        if request.is_ajax():
            original_filename = str(request.FILES['upl'])
            _type = ""
            if original_filename.startswith("fp"):
                _type = "type=%i&" % FLOOR_PLAN_IMAGE
            elif original_filename.startswith("sp"):
                _type = "type=%i&" % SITE_PLAN_IMAGE
            elif original_filename.startswith("thumb"):
                _type = ""
            else:
                _type = "type=%i&" % NORMAL_IMAGE

            custom_post = QueryDict('%stemp_hash=%s&file_name=%s' % (_type, request.GET.get('hash'), original_filename))
            file_form = AttachmentForm(custom_post, request.FILES)
            if file_form.is_valid():
                file_form.save()
                return HttpResponse('{"status":"success"}', content_type='application/json')
            return HttpResponse('{"status":"error: %s"}' % file_form.errors, content_type='application/json')

        return super(AdminImagesMultiUploadsView, self).dispatch(request, *args, **kwargs)


class AdminImagesListView(TemplateView, AdminView):

    template_name = 'list-images.html'

    def get_queryset(self):
        project = get_object_or_404(AdminProjects, id=self.args[0])
        self.project = project
        return AdminImages.objects.filter(project=project)

    def get_context_data(self, **kwargs):
        context = super(AdminImagesListView, self).get_context_data(**kwargs)
        context['project_id'] = self.args[0]
        project = get_object_or_404(AdminProjects, id=self.args[0])
        context['project_name'] = project.name
        context['project_date'] = project.project_date
        query_set = self.get_queryset()
        context['home_page_image'] = query_set.filter(type=HOMEPAGE_IMAGE).order_by('position')
        context['site_plan_image'] = list(query_set.filter(type=SITE_PLAN_IMAGE).all())
        context['floor_plan_images'] = list(query_set.filter(type=FLOOR_PLAN_IMAGE).all())
        context['normal_images'] = query_set.filter(type=NORMAL_IMAGE).order_by('pk')
        context['normal_images'] = query_set.filter(type=NORMAL_IMAGE).order_by('pk')
        context['images'] = list(query_set.filter(Q(type=NORMAL_IMAGE) | Q(type=HOMEPAGE_IMAGE)).all())

        context['images'].sort(cmp=sort_images(normal_image_regexp))
        context['site_plan_image'].sort(cmp=sort_images(sp_image_regexp))
        context['floor_plan_images'].sort(cmp=sort_images(fp_image_regexp))

        return context