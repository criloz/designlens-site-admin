from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.http import HttpResponseRedirect
from django.conf import settings
from .views import AdminImagesMultiUploadsView, AdminImagesListView, AdminImagesMultiSaveView,\
    AdminRePublishView, AdminUnPublishView, AdminPublishView, AdminSaveImageAjaxView, AdminImagesDeleteView

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'designlens2.views.home', name='home'),
    # url(r'^blog/', include('blog.urls'))

    url(r'^admin/', include(admin.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^$', lambda x: HttpResponseRedirect('/admin/')),
    url(r"^admin/image/list/(\d+)/$", AdminImagesListView.as_view(), name="image_list"),
    url(r"^admin/image/multiuploads/(\d+)/$", AdminImagesMultiUploadsView.as_view(), name="image_multiuploads"),
    url(r"^admin/image/multisave/(\d+)/$", AdminImagesMultiSaveView.as_view(), name="image_multisave"),
    url(r"^admin/publish/(\d+)/$", AdminPublishView.as_view(), name="publish_project"),
    url(r"^admin/unpublish/(\d+)/$", AdminUnPublishView.as_view(), name="unpublish_project"),
    url(r"^admin/republish/(\d+)/$", AdminRePublishView.as_view(), name="republish_project"),
    url(r"^admin/image/save/(\d+)/$", AdminSaveImageAjaxView.as_view(), name="save_image_ajax"),
    url(r"^admin/image/delete/(?P<pk>\d+)/$", AdminImagesDeleteView.as_view(success_url="/"), name="delete_image_ajax"),

    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),

)


if settings.ENVIRONMENT == "production":
    urlpatterns += patterns('', (r'^static/(?P<path>.*)$',
                                 'django.views.static.serve',
                                 {'document_root': settings.STATIC_ROOT}),
    )