__author__ = 'cristian'
from django.core.exceptions import MultipleObjectsReturned
from django.core.management.base import BaseCommand, CommandError
from django.http import HttpResponse
from django.conf import settings
from django.shortcuts import get_object_or_404
from ...models import AdminImages, Sites, AdminProjects, Projects, Profiles, Consultants,\
    Images, HORIZONTAL, VERTICAL_RIGHT_TEXT, DOUBLE_HORIZONTAL_RIGHT, DOUBLE_VERTICAL, \
    DOUBLE_HORIZONTAL_LEFT, VERTICAL_LEFT_TEXT, VERTICAL_CENTERED_TEXT, NORMAL_IMAGE, SITE_PLAN_IMAGE, \
    FLOOR_PLAN_IMAGE, HOMEPAGE_IMAGE

import json
from django.core.exceptions import ObjectDoesNotExist
import shutil
import os
from datetime import datetime
import uuid
from django.db.models import Q
from ...utils import *


class Command(BaseCommand):
    help = 'Get all the legacy project to the admin site'

    def handle(self, *args, **options):
        #get all the legacy projects
        legacy_projects = Projects.objects.filter(site_id=2).all()

        all_the_projects = legacy_projects.count()
        processed_project = 0
        for project in legacy_projects:

            try:
                new_project = AdminProjects.objects.get(prod_site_id=project.project_id,
                                                        site=Sites.objects.get(pk=project.site_id))
            except ObjectDoesNotExist:
                new_project = AdminProjects()

            except MultipleObjectsReturned:
                new_project = AdminProjects()
                AdminProjects.objects.filter(prod_site_id=project.project_id,
                                             site=Sites.objects.get(pk=project.site_id)).delete()

            #remove all the images of the project
            for image in AdminImages.objects.filter(project=new_project):
                if image.image:
                    try:
                        os.unlink(os.path.join(settings.BASE_DIR, settings.MEDIA_ROOT, str(image.image)))
                    except OSError:
                        pass

                if image.image_2:
                    try:
                        os.unlink(os.path.join(settings.BASE_DIR, settings.MEDIA_ROOT, str(image.image_2)))
                    except OSError:
                        pass
                image.delete()

            #save new project
            new_project.site = Sites.objects.get(pk=project.site_id)
            new_project.name = project.name
            new_project.prod_site_id = project.project_id
            new_project.architect = project.architect if project.architect else ""
            new_project.builder = project.builder if project.builder else ""
            new_project.interior_designer = project.interior_designer if project.interior_designer else ""
            new_project.project_date = str(project.project_date)
            new_project.location = project.location if project.location else ""
            new_project.flags = project.flags

            #save metawords
            if project.meta_words:
                new_project.meta_keywords = json.dumps(project.meta_words.strip().split())

            #save consultants
            consultants = Consultants.objects.filter(project_id=project.project_id, site_id=project.site_id).all()
            consultant_text = ""

            for i in consultants:
                consultant_text += i.consultants if i.consultants else ""

            new_project.consultants = consultant_text

            #save profile
            profiles = Profiles.objects.filter(project_id=project.project_id, site_id=project.site_id).all()
            profiles_text = ""
            for i in profiles:
                profiles_text += i.profile if i.profile else ""

            new_project.profile = profiles_text

            #get path
            project_id = str(new_project.prod_site_id)
            year = project_id[:4]
            month = project_id[4:6]
            _id = project_id[6:]

            directory = "{site}/{year}/{month}/{_id}".format(_id=_id, site=str(new_project.site.pk),
                                                             year=str(year), month=str(month))
            directory = os.path.join(settings.SHARED_ROOT, directory)
            new_project.path = directory

            new_project.save()

            normal_images_list = list()
            fp_images_list = list()
            sp_images_list = list()

            try:
                os.makedirs(os.path.join(settings.BASE_DIR, settings.MEDIA_ROOT, 'uploads'))
            except OSError:
                pass
            if os.path.exists(directory):
                paths = []
                #list files
                for file in os.listdir(directory):
                    #save normal images
                    match = normal_image_regexp.findall(file)
                    if match:
                        normal_images_list.append((int(match[0]), os.path.join(directory, file), file))
                        continue
                    match = fp_image_regexp.findall(file)
                    if match:
                        fp_images_list.append((int(match[0]), os.path.join(directory, file), file))
                        continue
                    match = sp_image_regexp.findall(file)
                    if match:
                        sp_images_list.append((int(match[0]), os.path.join(directory, file), file))
                        continue

                normal_images_list.sort()
                fp_images_list.sort()
                sp_images_list.sort()

                for fp_image in fp_images_list:
                    fileName, fileExtension = os.path.splitext(fp_image[1])
                    date_string = datetime.now().strftime("%Y_%m_%d_%H_%M")
                    image_name = date_string + "_" + str(uuid.uuid4())[:6] + fileExtension
                    image_new_path = os.path.join(settings.BASE_DIR, settings.MEDIA_ROOT, 'uploads', image_name)

                    image_new_url = os.path.join("uploads", image_name)

                    shutil.copy(fp_image[1], image_new_path)

                    new_image = AdminImages()
                    new_image.project = new_project
                    new_image.type = FLOOR_PLAN_IMAGE
                    new_image.image = image_new_url
                    new_image.file_name = fp_image[2]
                    new_image.save()

                for sp_image in sp_images_list:

                    fileName, fileExtension = os.path.splitext(sp_image[1])
                    date_string = datetime.now().strftime("%Y_%m_%d_%H_%M")
                    image_name = date_string + "_" + str(uuid.uuid4())[:6] + fileExtension
                    image_new_path = os.path.join(settings.BASE_DIR, settings.MEDIA_ROOT, 'uploads', image_name)

                    image_new_url = os.path.join("uploads", image_name)

                    shutil.copy(sp_image[1], image_new_path)

                    new_image = AdminImages()
                    new_image.project = new_project
                    new_image.type = SITE_PLAN_IMAGE
                    new_image.image = image_new_url
                    new_image.file_name = sp_image[2]
                    new_image.save()

                double_images = {}

                for normal_image in normal_images_list:

                    image_file = normal_image[1]
                    image_id = normal_image[0]
                    fileName, fileExtension = os.path.splitext(image_file)

                    try:
                        old_image = Images.objects.get(project_id=project.project_id, site_id=project.site_id,
                                                       image_id=int(str(project.project_id) + str(image_id)))
                    except ObjectDoesNotExist:
                        continue

                    date_string = datetime.now().strftime("%Y_%m_%d_%H_%M")
                    image_name = date_string + "_" + str(uuid.uuid4())[:6] + fileExtension
                    image_new_path = os.path.join(settings.BASE_DIR, settings.MEDIA_ROOT, 'uploads', image_name)

                    image_new_url = os.path.join("uploads", image_name)

                    shutil.copy(image_file, image_new_path)
                    new_image = AdminImages()

                    sibling = old_image.sibling

                    if project.cardinal == old_image.image_id:
                        new_project.cardinal = normal_image[2]
                        new_project.save()

                    if sibling and (str(sibling) in double_images):
                        double_images[str(sibling)].description_2 = old_image.description
                        image_keywords = old_image.meta_all.strip().split() if old_image.meta_all else []
                        double_images[str(sibling)].keywords_2 = json.dumps(image_keywords)
                        double_images[str(sibling)].image_2 = image_new_url
                        double_images[str(sibling)].file_name_2 = normal_image[2]
                        try:
                            sp = int(old_image.site_plan)
                        except:
                            sp = None

                        try:
                            fp = int(old_image.floorplan1)
                        except:
                            fp = None

                        double_images[str(sibling)].site_plan_2 = sp
                        double_images[str(sibling)].floor_plan_2 = fp
                        double_images[str(sibling)].save()


                    else:
                        double_images[str(old_image.image_id)] = new_image
                        new_image.description = old_image.description
                        new_image.project = new_project
                        new_image.layout = old_image.layout
                        image_keywords = old_image.meta_all.strip().split() if old_image.meta_all else []
                        new_image.keywords = json.dumps(image_keywords)
                        new_image.type =\
                            NORMAL_IMAGE if old_image.image_id != project.cardinal else HOMEPAGE_IMAGE
                        new_image.image = image_new_url
                        new_image.file_name = normal_image[2]

                        try:
                            sp = int(old_image.site_plan)
                        except:
                            sp = None

                        try:
                            fp = int(old_image.floorplan1)
                        except:
                            fp = None

                        new_image.site_plan = sp
                        new_image.floor_plan = fp

                        new_image.save()

            processed_project += 1
            completed = (processed_project*1.0/all_the_projects)*100
            print("%i %% completed" % completed)