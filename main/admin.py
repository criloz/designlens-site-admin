__author__ = 'cristian'

from .models import AdminProjects, AdminImages, DOUBLE_HORIZONTAL_RIGHT, \
    DOUBLE_VERTICAL, DOUBLE_HORIZONTAL_LEFT, NORMAL_IMAGE, HOMEPAGE_IMAGE, Users
from .utils import *
from django.forms.forms import NON_FIELD_ERRORS
from django.contrib import admin
from django import forms
from localflavor.us import forms as us_forms
from localflavor.us.models import STATE_CHOICES
from django.core.validators import RegexValidator
import json
from localflavor.us.us_states import US_STATES
from .widgets import TagEditWidget, UrlWidget
from django.db.models import Q
from .templatetags.designlens import meta_keywords_autocomplete

HOUSING_TYPE_LIST = ("Condo", "Townhouse", "High Density", "Under 10 per acre", "10-15 per acre",
                    "15-20 per acre", "20+ per acre", "Urban High Density", "Single-family Detached",
                    "Conventional Lot", "Affordable Starter", "First-time Move-up", "Second/Third-Time Move-up",
                    "Luxury", "Small Lot Detached", "Alley-fed Neotraditional", "Cluster", "Compact Lots - Under",
                    "4500 sq. ft.", "Duplex", "Two-pack",)


class AdminProjectsAdmin(admin.ModelAdmin):

    list_display = ('name', 'project_date', 'architect', 'builder', 'location', "site", 'prod_site_id')
    readonly_fields = ('prod_site_id', 'path', 'site')

    def get_form(self, request, obj=None, **kwargs):

        first_field = ['name', 'project_date', 'architect', 'builder', "interior_designer"]

        class AdminProjectsForm(forms.ModelForm):
            class Meta:
                model = AdminProjects

            def full_clean(self):
                #save keywords
                keywords = []
                keywords_fields = ["square_footage_keywords",
                                   "lot_size_keywords",
                                   "density_keywords",
                                   "region_keywords",
                                   "housing_keywords",
                                   "other_meta_keywords"]

                for kf in keywords_fields:
                    value = self.data.get(kf, "").strip()
                    try:
                        keys = json.loads(value)
                    except:
                        keys = value.split(" ")
                    keywords.extend(keys)
                city = self.data.get("city", "")

                if city:
                    keywords.append(city)

                state = self.data.get("state", "")
                state = dict(US_STATES).get(state, "")
                if state:
                    keywords.append(state)

                if "meta_keywords" not in self.data:
                    self.data["meta_keywords"] = json.dumps(keywords)

                #save location and profile
                profile = list()

                if city and state:
                    self.data["location"] = city + ", " + state
                    address_field = self.data.get("address", "")
                    _zip = self.data.get("zip", "")
                    phone = self.data.get("phone", "")
                    address = list()
                    address.append("{0}".format(address_field))
                    address.append("{0}, {1} {2}".format(city, state, _zip))
                    if phone:
                        address.append("Phone: {0}".format(phone))

                    profile.append("<br>".join(address)+"<br>")

                pre_profile = self.data.get("pre_profile", "").strip()

                if pre_profile:
                    profile.append(pre_profile)

                if "profile" not in self.data:
                    self.data["profile"] = "<br>".join(profile)

                super(AdminProjectsForm, self).full_clean()
                print self._errors


        if obj is None:
            AdminProjectsForm.base_fields["city"] = forms.CharField(max_length=255, required=False)
            AdminProjectsForm.base_fields["address"] = forms.CharField(max_length=255,  required=False)
            AdminProjectsForm.base_fields["state"] = us_forms.USStateField(widget=forms.Select(choices=STATE_CHOICES),
                                                                           required=False)
            AdminProjectsForm.base_fields["phone"] = us_forms.USPhoneNumberField(required=False)
            AdminProjectsForm.base_fields["zip"] = forms.CharField(max_length=5,  required=False)
            first_field.extend(["city", "address", "state", "phone", "zip"])

        AdminProjectsForm.base_fields["project_date"] = forms.CharField(max_length=6,
                                                                        validators=
                                                                        [RegexValidator(regex="^[0-9]+$",
                                                                                        message="only numbers")])

        if obj is None:

            AdminProjectsForm.base_fields["pre_profile"] = forms.CharField(widget=forms.Textarea, label="Profile",
                                                                           required=False)
            other_fields = ["pre_profile"]

        else:
            other_fields = ["profile"]

        if obj is None:
            AdminProjectsForm.base_fields["square_footage_keywords"] = forms.CharField(widget=TagEditWidget,
                                                                                       required=False)
            AdminProjectsForm.base_fields["lot_size_keywords"] = forms.CharField(widget=TagEditWidget, required=False)
            AdminProjectsForm.base_fields["density_keywords"] = forms.CharField(widget=TagEditWidget, required=False)
            AdminProjectsForm.base_fields["region_keywords"] = forms.CharField(widget=TagEditWidget, required=False)
            AdminProjectsForm.base_fields["housing_keywords"] = \
                forms.CharField(widget=TagEditWidget(taget_edit_attrs={"auto_complete": HOUSING_TYPE_LIST}),
                                required=False,  label="Housing type keywords")
            AdminProjectsForm.base_fields["other_meta_keywords"] = forms.CharField(widget=TagEditWidget,
                                                                                   label="Other keywords",
                                                                                   required=False)

            other_fields.extend(["square_footage_keywords", "lot_size_keywords", "density_keywords", "region_keywords",
                                 "housing_keywords", "other_meta_keywords"])

        if obj is not None:
            first_field.append("location")
            AdminProjectsForm.base_fields["meta_keywords"] = forms.CharField(
                widget=TagEditWidget(taget_edit_attrs={"auto_complete": meta_keywords_autocomplete()}), required=False)
            image_list = list(AdminImages.objects.filter(project=obj).
                              filter(Q(type=NORMAL_IMAGE) | Q(type=HOMEPAGE_IMAGE)).all())

            image_list.sort(cmp=sort_images(normal_image_regexp))
            choises = [["", ""]]
            for image_obj in image_list:
                choises.append([image_obj.file_name, image_obj.file_name])
                if image_obj.file_name_2:
                    choises.append([image_obj.file_name_2, image_obj.file_name_2])

            AdminProjectsForm.base_fields["cardinal"] = forms.ChoiceField(choices=choises, required=False)
            other_fields.append("meta_keywords")
            first_field.append("cardinal")

        published_fields = None

        if obj is not None and obj.prod_site_id is not None:
            AdminProjectsForm.base_fields["project_url"] = forms.CharField(widget=UrlWidget,
                                                                           required=False,
                                                                           initial="http://designlens.com/layout?next=1&site_id=2&iid=%s1" % obj.prod_site_id)
            published_fields = [("Live site project",
                                {"fields": ["prod_site_id", "site", "project_url"]})]

        first_field.append("consultants")
        first_field.append("flags")
        fieldsets = [
            (None, {
                "fields": first_field
            }),
            ("",
                {"fields": other_fields})
        ]
        if published_fields:
            published_fields.extend(fieldsets)
            fieldsets = published_fields

        self.fieldsets = fieldsets

        return AdminProjectsForm


admin.site.register(AdminProjects, AdminProjectsAdmin)


class AdminImagesAdmin(admin.ModelAdmin):
    list_display = ('project', 'type', 'layout', 'image', 'keywords', 'description', 'file_name', 'file_name_2')

    def get_form(self, request, obj=None, **kwargs):
        self.fieldsets = (
            (None, {
                "fields": ('project', 'image', 'type')
            }),
            ("Image 1 Data", {
                "fields": ('description', 'layout', 'keywords', 'file_name')
            }),
            ('Image 2 Data: only needed if a double image layout was selected', {
                'classes': ('collapse',),
                "fields": ('image_2', "description_2", "keywords_2", "file_name_2")
            }),
        )

        class AdminImagesForm(forms.ModelForm):
            class Meta:
                model = AdminImages

            def add_error(self, field, msg):
                field = field or NON_FIELD_ERRORS

                if field in self._errors:
                    self._errors[field].append(msg)
                else:
                    self._errors[field] = self.error_class([msg])

            def validate_required_field(self, cleaned_data, field_name, message="This field is required"):
                if field_name in cleaned_data and not cleaned_data[field_name]:
                    self.add_error(field_name, message)

            def validate_not_required_field(self, cleaned_data, field_name, message="This field is not required"):
                if field_name in cleaned_data and cleaned_data[field_name]:
                    self.add_error(field_name, message)

            def clean(self):
                cleaned_data = super(AdminImagesForm, self).clean()
                _type = cleaned_data.get("type", None)

                if _type == NORMAL_IMAGE:
                    self.validate_required_field(cleaned_data, "layout")
                    layout = cleaned_data.get("layout", None)
                    """
                    if layout is not None:
                        if layout in (DOUBLE_HORIZONTAL_RIGHT, DOUBLE_VERTICAL, DOUBLE_HORIZONTAL_LEFT):
                            self.validate_required_field(cleaned_data, "image_2",
                                                         message="This layout need an additional image")
                        else:
                            self.validate_not_required_field(cleaned_data, "image_2",
                                                             message="Invalid field for the selected layout")
                            self.validate_not_required_field(cleaned_data, "description_2",
                                                             message="Invalid field for the selected layout")
                            self.validate_not_required_field(cleaned_data, "keywords_2",
                                                             message="Invalid field for the selected layout")"""
                else:
                    self.validate_not_required_field(cleaned_data, "description",
                                                     message="Invalid field for the selected image type")
                    self.validate_not_required_field(cleaned_data, "keywords",
                                                     message="Invalid field for the selected image type")
                    self.validate_not_required_field(cleaned_data, "image_2",
                                                     message="Invalid field for the selected image type")
                    self.validate_not_required_field(cleaned_data, "description_2",
                                                     message="Invalid field for the selected image type")
                    self.validate_not_required_field(cleaned_data, "keywords_2",
                                                     message="Invalid field for the selected image type")
                return cleaned_data

        return AdminImagesForm


admin.site.register(AdminImages, AdminImagesAdmin)


class UsersAdmin(admin.ModelAdmin):
    list_display = ('username', 'company_name', 'sub_date', 'flags', 'access_level', 'last_logged_in', 'site_id')
    readonly_fields = ('site_id', )
    exclude = ("password_md5", )

    def get_queryset(self, request):
        qs = super(UsersAdmin, self).get_queryset(request)
        return qs.filter(site_id=2)
admin.site.register(Users, UsersAdmin)

