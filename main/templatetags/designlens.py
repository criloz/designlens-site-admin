__author__ = 'cristian'
from django.utils.html import mark_safe
from django import template
from ..models import LAYOUT_CHOICES_DICT, AdminProjects, AdminImages
from django.core.cache import cache
import json
import HTMLParser

register = template.Library()


def safe_description(value):
    return mark_safe(value.replace('"', '\\"').replace('\n', '\\n').replace('\r', '\\r'))


def get_layout(value):
    return LAYOUT_CHOICES_DICT[value]


def get_keys(value):

    try:
        keys = json.loads(value)
    except:
        keys = []
    h = HTMLParser.HTMLParser()
    return mark_safe(h.unescape(' '.join(keys)).replace('"', '\\"'))

register.filter(get_layout)
register.filter(get_keys)
register.filter(safe_description)

@register.simple_tag
def admin_site_consultants_autocomplete():
    autocomplete_consultants = cache.get("autocomplete_consultants")

    try:
        autocomplete_consultants = json.loads(autocomplete_consultants)
    except:
        autocomplete_consultants = None

    if not autocomplete_consultants:
        consultants = {"architects": [], "builders": [], "interior_designers": []}
        architects_list = list()
        builders_list = list()
        interior_designers_list = list()

        id_counter = 1
        for project in AdminProjects.objects.all():
            architect_text = project.architect
            builder_text = project.builder
            interior_designer_text = project.interior_designer

            if architect_text and (architect_text not in architects_list):
                architects_list.append(architect_text)
                architect = {"id": id_counter, "label": architect_text, "value": architect_text}
                consultants["architects"].append(architect)

            if builder_text and (builder_text not in builders_list):
                builders_list.append(builder_text)
                builder = {"id": id_counter, "label": builder_text, "value": builder_text}
                consultants["builders"].append(builder)

            if interior_designer_text and (interior_designer_text not in interior_designers_list):
                interior_designers_list.append(interior_designer_text)
                interior_designer = {"id": id_counter, "label": interior_designer_text, "value": interior_designer_text}
                consultants["interior_designers"].append(interior_designer)

            id_counter += 1
        cache.set("autocomplete_consultants", json.dumps(consultants), 60*60*24)
        autocomplete_consultants = consultants

    return mark_safe("""
            architects_autocomplete = {0};
            builders_autocomplete = {1};
            interior_designers_autocomplete = {2};
    """.format(json.dumps(autocomplete_consultants["architects"]), json.dumps(autocomplete_consultants["builders"]),
               json.dumps(autocomplete_consultants["interior_designers"])))


@register.simple_tag
def location_autocomplete():
    autocomplete_location = cache.get("autocomplete_location")

    try:
        autocomplete_location = json.loads(autocomplete_location)
    except:
        autocomplete_location = None

    if not autocomplete_location:
        autocomplete_location = []
        all_keywords = set()

        for project in AdminProjects.objects.all():
            location = project.location
            if location.strip():
                all_keywords.add(location)

        id_counter = 1
        for key in all_keywords:
            new_key = {"id": id_counter, "label": key, "value": key}
            autocomplete_location.append(new_key)
            id_counter += 1

        cache.set("autocomplete_location", json.dumps(autocomplete_location), 60*60*24)

    return mark_safe("""
                autocomplete_location = {0};
        """.format(json.dumps(autocomplete_location)))


@register.simple_tag
def image_keywords_autocomplete():
    autocomplete_image_keywords = cache.get("autocomplete_image_keywords")

    try:
        autocomplete_image_keywords = json.loads(autocomplete_image_keywords)
    except:
        autocomplete_image_keywords = None

    if not autocomplete_image_keywords:
        autocomplete_image_keywords = []
        all_keywords = set()

        for image in AdminImages.objects.all():
            keyword = image.keywords if image.keywords else ""
            keyword_2 = image.keywords_2 if image.keywords_2 else ""

            try:
                keyword = json.loads(keyword)
            except:
                keyword = []

            try:
                keyword_2 = json.loads(keyword_2)
            except:
                keyword_2 = []

            all_keywords = all_keywords.union(set(keyword), set(keyword_2))

        id_counter = 1
        for key in all_keywords:
            new_key = {"id": id_counter, "label": key, "value": key}
            autocomplete_image_keywords.append(new_key)
            id_counter += 1

        cache.set("autocomplete_image_keywords", json.dumps(autocomplete_image_keywords), 60*60*24)

    return mark_safe("""
                autocomplete_image_keywords = {0};
        """.format(json.dumps(autocomplete_image_keywords)))


def meta_keywords_autocomplete():
    autocomplete_meta_keywords = cache.get("autocomplete_meta_keywords")

    try:
        autocomplete_meta_keywords = json.loads(autocomplete_meta_keywords)
    except:
        autocomplete_meta_keywords = None

    if not autocomplete_meta_keywords:
        autocomplete_meta_keywords = set()

        for project in AdminProjects.objects.all():
            keyword = project.meta_keywords if project.meta_keywords else ""

            try:
                keyword = json.loads(keyword)
            except:
                keyword = []

            autocomplete_meta_keywords = autocomplete_meta_keywords.union(set(keyword))

        cache.set("autocomplete_meta_keywords", json.dumps(list(autocomplete_meta_keywords)), 60*60*24)

    return autocomplete_meta_keywords